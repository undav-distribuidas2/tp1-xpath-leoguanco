package com.distribuida.app;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.filter.Filters;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

public class XpathReader {

    private Document getJdomDocument;
    private XMLOutputter xmlOutputter;
    private XPathFactory xPathFactory;

    XpathReader(String xmlFile) throws JDOMException, IOException {
        SAXBuilder getJdomBuilder = new SAXBuilder();
        this.getJdomDocument = getJdomBuilder.build(xmlFile);
        this.xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
        this.xPathFactory = XPathFactory.instance();
    }

    public String getDocument() throws IOException {
        StringWriter writer = new StringWriter();
        xmlOutputter.output(this.getJdomDocument, writer);

        return writer.toString();
    }

    public String get(String xpath) throws IOException {
        String value = null;

        if (xpath.contains("text()")){
            value = this.getValue(xpath);
        } else if (xpath.contains("/@")){
            value = this.getAttribute(xpath);
        }
        else {
            value = this.getElement(xpath);
        }

        return value;
    }

    private String getAttribute(String xpath) throws IOException {
        StringWriter writer = new StringWriter();

        XPathExpression<Attribute> expression = xPathFactory.compile(xpath, Filters.attribute());
        List<Attribute> elementList = expression.evaluate(this.getJdomDocument);
        for (Attribute element : elementList){
            writer.append(element.getValue());
            writer.append("\n");
        }

        return writer.toString();
    }

    private String getValue(String xpath) throws IOException {
        StringWriter writer = new StringWriter();

        if (xpath.contains("/text()")){
            xpath = xpath.replace("/text()", "");
        }

        XPathExpression<Element> expression = xPathFactory.compile(xpath, Filters.element());
        List<Element> elementList = expression.evaluate(this.getJdomDocument);
        for (Element element : elementList){
            writer.append(element.getValue());
            writer.append("\n");
        }

        return writer.toString();
    }

    private String getElement(String xpath) throws IOException {
        StringWriter writer = new StringWriter();

        XPathExpression<Element> expression = xPathFactory.compile(xpath, Filters.element());
        List<Element> elementList = expression.evaluate(this.getJdomDocument);
        for (Element element : elementList){
            xmlOutputter.output(element, writer);
            writer.append("\n");
        }

        return writer.toString();
    }
}
