package com.distribuida.app;

import org.jdom2.JDOMException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class XpathView extends JFrame implements ActionListener{
    private JFrame jFrame;
    private JPanel jPanel;
    private JTextArea jTextAreaInit;
    private JTextArea jTextAreaResult;
    private JScrollPane jScrollPaneInit;
    private JScrollPane jScrollPaneResult;
    private JTextField jTextFieldPath;
    private JButton jButtonSearch;
    private XpathReader xpathReader;

    public XpathView() throws JDOMException, IOException {
        this.xpathReader = new XpathReader("ventas.xml");

        this.jFrame = new JFrame("UNDAV - Distribuida II");
        this.jPanel = new JPanel();
        this.jPanel.setBounds(0,0,1064,848);
        this.jPanel.setLayout(null);

        this.jTextFieldPath = new JTextField();
        this.jTextFieldPath.setBounds(40, 20, 800, 20);

        this.jButtonSearch = new JButton("Buscar");
        this.jButtonSearch.setBounds(940, 20, 100, 20);
        this.jButtonSearch.addActionListener(this);

        this.jTextAreaInit = new JTextArea();
        this.jTextAreaInit.setText(this.xpathReader.getDocument());
        this.jTextAreaInit.setEnabled(false);

        this.jTextAreaResult = new JTextArea();
        this.jTextAreaResult.setText("");
        this.jTextAreaResult.setEnabled(false);

        this.jScrollPaneInit = new JScrollPane();
        this.jScrollPaneInit.setBounds(40, 80, 472, 588);
        this.jScrollPaneInit.setViewportView(jTextAreaInit);

        this.jScrollPaneResult = new JScrollPane();
        this.jScrollPaneResult.setBounds(550, 80, 472, 588);
        this.jScrollPaneResult.setViewportView(jTextAreaResult);;

        this.jPanel.add(this.jTextFieldPath);
        this.jPanel.add(this.jButtonSearch);
        this.jPanel.add(this.jScrollPaneInit);
        this.jPanel.add(this.jScrollPaneResult);

        this.jFrame.getContentPane().add(jPanel);
    }

    public void launchFrame() {
        this.jFrame.setSize(1064,768);
        this.jFrame.getContentPane().setBackground(Color.white);
        this.jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.jFrame.setVisible(true);
    }


    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String result = null;
        try {
            result = this.xpathReader.get(this.jTextFieldPath.getText());
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.jTextAreaResult.setText(result);
    }
}
